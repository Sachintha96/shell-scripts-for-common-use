# Shell scripts for common use

- **sys-update.sh** - apt update, upgrade, and auto-remove unnecessary packages from a single execution. No need to interact with shell again(say y/n) after executing the script.

- **snap-clean.sh** - clean unnecessary snap files

- **tmpmail.sh** - temporary mails handling script
